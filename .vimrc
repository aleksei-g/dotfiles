call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'Valloric/YouCompleteMe'
Plug 'jiangmiao/auto-pairs'
Plug 'easymotion/vim-easymotion'
Plug 'airblade/vim-gitgutter'
Plug 'rking/ag.vim'
"colorshemes
Plug 'morhetz/gruvbox'

call plug#end()

syntax on
let g:mapleader=';'
colorscheme gruvbox
set background=dark
set number
set textwidth=79
set shiftwidth=4
set expandtab
set tabstop=4
set softtabstop=4
set shiftround
set autoindent
set hlsearch
set incsearch  
set clipboard=unnamed

"mappings
"   mappings nerdtree
map <C-n> :NERDTreeToggle<CR>
"   mappings vim-easymotion'
map <Leader> <Plug>(easymotion-prefix)
"      <Leader>S{char}{char} to move to {char}{char}
map <Leader>S <Plug>(easymotion-overwin-f2)
"      <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)
"      Move to line
map <Leader>l <Plug>(easymotion-bd-jk)
nmap <Leader>l <Plug>(easymotion-overwin-line)"
"      Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)"

"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>
